<?php

namespace Sharecoto;
class FunctionProxy
{
    public function __call($method, $args)
    {
        return $this->call($method, $args);
    }

    public static function __callStatic($method, $args)
    {
        $my = new self;
        return $my->call($method, $args);
    }

    private function call($method, $args)
    {
        if (!$this->functionExists($method)) {
            throw new \BadMethodCallException('Call to undefined function ' .$method);
        }
        return call_user_func_array('\\' . $method, $args);
    }

    /**
     * function_exists()のラッパー
     */
    public function functionExists($method)
    {
        return function_exists('\\'. $method);
    }
}

