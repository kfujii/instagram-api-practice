<?php
/**
 * phpunit用
 */
date_default_timezone_set('Asia/Tokyo');

putenv("APPLICATION_ENV=testing");
require(__DIR__ . '/../vendor/autoload.php');

define('APPLICATION_ENV', 'testing');
define('APPLICATION_PATH', realpath(__DIR__ . '/../'));

// DB設定
