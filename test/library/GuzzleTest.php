<?php
/**
 * Guzzleを使ってみるためのテスト
 */
use GuzzleHttp\Client;

class GuzzleTest extends PHPUnit_Framework_TestCase
{
    public function testCreateClient()
    {
        $client = new Client();

        //$res = $client->get('http://google.co.jp');
        $request = $client->createRequest('GET', 'http://httpbin.org/user-agent');
        $response = $client->send($request);

        $body = $response->getBody();
    }

    public function testPostBody()
    {
        $client = new Client();

        $request = $client->createRequest('POST', 'http://httpbin.org/post');
        $postBody = $request->getBody();
        $postBody->setField('foo', 'bar');
        $postBody->addFile(new \GuzzleHttp\Post\PostFile('file', fopen(__FILE__, 'r')));

        $res = $client->send($request);
    }

    /**
     * @expectedException GuzzleHttp\Exception\ServerException
     */
    public function testException()
    {
        $client = new Client();

        $request = $client->createRequest('GET', 'http://httpbin.org/status/500');
        $res = $client->send($request);

    }

    /**
     * Instagramにsubscriptionコールしてみる
     */
    /*
    public function testInstaSubscription()
    {
        $params = [
            'body' =>[
                'client_id' => '494426d5183f46b2ba3f8227b52500d6',
                'client_secret' => '51b42d3d67994c0f8b485e6e957380ee',
                'object' => 'location',
                'object_id' => '1257285',
                'aspect' => 'media',
                'verify_token' => 'rRYZXKcLeHpx3d6WRRJD1noNd3UaQGxB',
                'callback_url' => 'https://dev-insta-api-practice.herokuapp.com/rt',
            ]
        ];

        $client = new Client();
        $response = $client->post('https://api.instagram.com/v1/subscriptions/', $params);

        $body = $response->getBody();
        echo $body;
    }
     */

    /**
     * Subscriptions List
     */
    public function testGetSubscriptions()
    {
        $params = [
            'query' =>[
                'client_id' => '494426d5183f46b2ba3f8227b52500d6',
                'client_secret' => '51b42d3d67994c0f8b485e6e957380ee',
            ]
        ];

        $client = new Client();
        $response = $client->get('https://api.instagram.com/v1/subscriptions/', $params);
        echo $response->getBody();
    }

    /*
    public function testDeleteAllSubscription()
    {
        $params = [
            'query' =>[
                'client_id' => '494426d5183f46b2ba3f8227b52500d6',
                'client_secret' => '51b42d3d67994c0f8b485e6e957380ee',
                'object' => 'all'
            ]
        ];
        $client = new Client();
        $response = $client->delete('https://api.instagram.com/v1/subscriptions/', $params);
        echo $response->getBody();
    }
     */
}
