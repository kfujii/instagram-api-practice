<?php
use Instaphp\Instaphp;

class InstaTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $config = require(__DIR__ . '/../../configs/instagram.php');
        $this->insta = new Instaphp($config);
    }

    public function testUsersSeach()
    {
        $insta = $this->insta;

        $name = 'fujksk';

        $result = $insta->Users->Search($name);
        var_dump($result);
        $this->assertInstanceOf('Instaphp\Instagram\Response', $result);
    }

    /*
    public function testUsersInfo()
    {
        $insta = $this->insta;
        $id = '477671449';

        $user = $insta->Users->Info($id);
    }

    public function testLocationSearch()
    {
        $geo_id = '788029';

        $insta = $this->insta;

        $results = $insta->Locations->Search(['facebook_places_id'=>'188380191206731']);
    }

    public function testKourakuenHall()
    {
        // 後楽園ホールのLocationID
        $id = '94369';
        $results = $this->insta->Locations->Recent($id);

    }
     */
}
