<?php
/**
 * instagramアプリ用設定ファイル。
 * 以下に必要な情報をセットして、
 * instagra.phpにリネームして使うといいよ
 */
return [
    'client_id' => '',
    'client_secret' => '',
    'url' => '',
    'redirect_url' => '',
];
