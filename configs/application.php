<?php
/**
 * Slimの設定
 */

return array (
    'templates.path' => __DIR__ . '/../templates',
    'view' => new \Slim\Views\Twig,
    'ssl_require' => true,
    'mode' => 'production',
    'jquery' => '2.1.1',
    'configPath' => realpath(__DIR__),
);

