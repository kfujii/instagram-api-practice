<?php

namespace AppFrontController {
    ini_set('display_errors', 'on');
    error_reporting(E_ALL);
    date_default_timezone_set('Asia/Tokyo');
    require(__DIR__ . '/vendor/autoload.php');

    const REALTIME_TOKEN = 'rRYZXKcLeHpx3d6WRRJD1noNd3UaQGxB';

    $config = require(__DIR__ . '/configs/application.php');
    $app = new \Slim\Slim($config);
    $app->setName('default');
    $app->instagram = new \Instaphp\Instaphp(require(__DIR__ . '/configs/instagram.php'));
    $app->proxy = new \Sharecoto\FunctionProxy;

    $app->get('/', function() {
        $app = \Slim\Slim::getInstance('default');
        $medias = $app->instagram->Media->Popular();
        $app->render(
            'index.twig',
            [
                'medias' => $medias->data,
                'app' => $app,
                'title' => 'Popular',
                'lead' => 'Get a list of what media is most popular at the moment. Can return mix of image and video types.'
            ]
        );
    });

    /**
     * Real-Time Subscription CallbackUrl
     */
    $app->get('/rt', function() {
        $app = getSlim();
        $token = REALTIME_TOKEN;

        $get = $app->request->get();
        if (empty($get['hub_challenge'])) {
            return;
        }

        $client = new \GuzzleHttp\Client();
        echo $get['hub_challenge'];
    });

    $app->post('/rt', function() {
        $app = getSlim();
        $postData = $app->request->getBody();
        error_log(var_export($postData, true));
    });

    $app->get('/user', function() {
        $app = \Slim\Slim::getInstance('default');
        $medias = $app->instagram->Users->Recent('477671449');
        $app->render(
            'index.twig',
            [
                'medias' => $medias->data,
                'app' => $app,
                'title' => 'ユーザーの投稿を取得',
                'lead' => '藤井の投稿を取得'
            ]
        );

    });

    $app->get('/tag', function() {
        $app = \Slim\Slim::getInstance('default');
        $medias = $app->instagram->Tags->Recent('iceribbon');
        $app->render(
            'index.twig',
            [
                'medias' => $medias->data,
                'app' => $app,
                'title' => 'タグ"#iceribbon"で検索',
                'lead' => ''
            ]
        );
    });

    $app->get('/tag_search', function() {
        $app = getSlim();
        $get = $app->request->get();
        $medias = [];
        if (isset($get['tag'])) {
            $medias = $app->instagram->Tags->Recent(urlencode($get['tag']), ['count'=>50])->data;
        }
        $app->render(
            'index.twig',
            [
                'medias' => $medias,
                'app' => $app,
                'title' => '動的にタグで検索',
                'lead' => '',
                'form' => 'tag_form.twig',
            ]
        );
    });

    $app->get('/user_search', function() {
        $app = \Slim\Slim::getInstance('default');

        $get = $app->request->get();
        $users = [];
        if (isset($get['username'])) {
            $users = $app->instagram->Users->Search($get['username'])->data;
        }

        $app->render(
            'user_search.twig',
            [
                'users' => $users,
                'app' => $app,
                'title' => 'ユーザー検索',
                'lead' => ''
            ]
        );
    });

    $app->get('/user_medias', function() {
        $app = \Slim\Slim::getInstance('default');

        $get = $app->request->get();
        $medias = [];
        if (isset($get['id'])) {
            $medias = $app->instagram->Users->Recent($get['id'])->data;
        }

        $app->render(
            'index.twig',
            [
                'medias' => $medias,
                'app' => $app,
                'title' => 'ユーザーIDで検索',
                'lead' => ''
            ]
        );
    });

    $app->get('/korakuenhall', function() {
        $app = \Slim\Slim::getInstance('default');

        $location = '94369';// 後楽園ホールのLocation ID
        $medias = $app->instagram->Locations->Recent($location)->data;

        $app->render(
            'index.twig',
            [
                'medias' => $medias,
                'app' => $app,
                'title' => 'location-idで検索。「後楽園ホール」の投稿',
                'lead' => 'location-idは、/locations/searchで調べておく必要がある。'
            ]
        );
    });

    $app->get('/miumiu', function() {
        $app = \Slim\Slim::getInstance('default');

        $location = '24182118';// Location ID
        $medias = $app->instagram->Locations->Recent($location)->data;

        $app->render(
            'index.twig',
            [
                'medias' => $medias,
                'app' => $app,
                'title' => 'location-idで検索。',
                'lead' => 'ミュウミュウ青山店のロケーションIDは複数登録されてるが、このサンプルはそのうち「miumiu 青山店」で登録されているものを使用'
            ]
        );
    });

    $app->run();

    function getSlim($name = 'default')
    {
        return \Slim\Slim::getInstance($name);
    }
}
